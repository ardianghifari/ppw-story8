var favorite = 0;

function search() {
    var search = $("input").val();
    $.ajax({
        url: "books/" + search,
        success: function(result) {            
            $.each(result.items, function(i, item) {
                $("<tr>").append(
                    $("<td>").append(i+1),
                    $("<td>").text(item.volumeInfo.title),
                    $("<td>").text(item.volumeInfo.authors),
                    $("<td>").text(item.volumeInfo.publishedDate),
                    $("<td>").text(item.volumeInfo.description),
                    $("<td>").append("<img src=" + item.volumeInfo.imageLinks.thumbnail + " >"),
                    $("<td>").append("<i id=\"star-" + i  + "\" onclick=\"turnYellow(" + i + ")\" class=\"fas fa-star\"></i>"),
                ).appendTo("tbody");
            });
        }
    }); 
}

function turnYellow(id) {
    if ($("#star-" + id).css("color") === "rgb(255, 220, 0)") {
        $("#star-" + id).css("color", "#111111");
        $("#favorite").text("Favorite(s): " + --favorite);
    } else {
        $("#star-" + id).css("color", "rgb(255, 220, 0)");
        $("#favorite").text("Favorite(s): " + ++favorite);
    }
}