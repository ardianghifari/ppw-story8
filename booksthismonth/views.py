from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

# Create your views here.

def get_books(request, search):
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + search
	books = json.loads(requests.get(url).text)
	return JsonResponse(books)

def view_booksthismonth(request):
	return render(request, 'Booksthismonth.html')