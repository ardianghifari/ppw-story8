from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('books/<search>', get_books, name='books'),
	path('', view_booksthismonth, name='booksthismonth'),
]