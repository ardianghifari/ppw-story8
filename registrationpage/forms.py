from django import forms
from .models import Subscriber

class Subscribe_Form(forms.ModelForm):
	name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
	email = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
	password = forms.CharField(max_length=32, widget=forms.PasswordInput())

	class Meta:
		model = Subscriber
		fields = ['name', 'email', 'password']