from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('create', create_subscriber, name='create_subscriber'),
	path('subscribers', get_subscribers, name='get_subscribers'),
	path('', view_registrationpage, name='registrationpage'),
]