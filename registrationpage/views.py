from django.shortcuts import render, reverse
from django.core import serializers
from django.http import HttpResponse
from django.http import JsonResponse
from .forms import Subscribe_Form
from .models import Subscriber
import json
import requests

# Create your views here.

def make_list_subscribers():
	fields = list(Subscriber.objects.values())
	subscribers = {'fields':fields}
	return JsonResponse(subscribers)

def get_subscribers(request):
	return make_list_subscribers()

def create_subscriber(request):
	if request.method == 'POST':
		name = request.POST['name']
		email = request.POST['email']
		password = request.POST['password']
		Subscriber.objects.create(name=name, email=email, password=password)
	return make_list_subscribers()

def view_registrationpage(request):
	form = Subscribe_Form()
	return render(request, 'Registrationpage.html', {'form':form})