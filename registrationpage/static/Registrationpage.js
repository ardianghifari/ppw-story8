$(document).ready(function(e){

    $("#subscribe-button").attr("disabled", "disabled")

    $("#id_email").change(function(){
        var this_email = this.value;
        var used = false;
        $.ajax({
            url: "subscribers",
            success: function(result) {
                for(var i = 0; i < result["fields"].length; i++) {
                    if (result["fields"][i]["email"] == this_email) {
                        used = true;
                        return !used;
                    }
                }
                if (used) {
                    $("#email-alert").append("Email has been used. Please try another email.");                  
                } else {
                    $("#email-alert").append("Email can be used.");
                    if (($("#id_password").val().length >= 8) && ($("#id_name").val().length > 0)) {
                        $('#subscribe-button').prop("disabled", true);
                        $("#subscribe-button").removeAttr("disabled");
                    }
                }               
            }
        });
    });

    $("#subscribe-button").on('click',function(e) {
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "create",
            data: {
                name: $("#id_name").val(),
                email: $("#id_email").val(),
                password: $("#id_password").val(),
                csrfmiddlewaretoken: $("input[name=csrfmiddlewaretoken]").val()
            },
            success: function(result) {
                alert("Data has been saved");
                $("#id_name").val("");
                $("#id_email").val("");
                $("#id_password").val("");
                $("#email-alert").empty();
            }
        });
    })

});