import time
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import view_profilepage

# Create your tests here.

class ProfilepageTest(TestCase):

	def test_landingpage_url_exists(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_landingpage_using_profilepage_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'Profilepage.html')

