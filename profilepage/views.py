from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.contrib.auth import logout
from django.urls import reverse

# Create your views here.

def view_profilepage(request):
	return render(request, 'Profilepage.html')

def logout_view(request):
	logout(request)
	request.session.flush()
	return HttpResponseRedirect(reverse("home:profilepage"))