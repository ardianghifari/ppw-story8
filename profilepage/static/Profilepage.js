function showPage() {
  $(".loader").hide();
  $("#main").show();
}


$(document).ready(function(){

	setTimeout(showPage, 3000);

    $("#change-theme").click(function(){
    	if ($(this).text() == "Kami cinta fasilkom") {
    		$(this).text("Tampilan awal");
	        $(".small-rectangle").css("background-color", "red");
	        $(".big-rectangle").css("background-color", "#0b0f8e"); 
	        $("#about-title").css("color", "#0b0f8e");
	        $("#experience-title").css("color", "red");
	        $("#skill-title").css("color", "#0b0f8e");
	        $("#education-title").css("color", "red");
	        $(".text-paragraph1").css("color", "#0b0f8e");  
	        $(".text-paragraph2").css("color", "red");
	        $("h4, h5").css("color", "#f2f2f2");
    	} else {
    		$(this).text("Kami cinta fasilkom");
    		$(".small-rectangle").css("background-color", "#f5f5f5");    		
    		$(".big-rectangle").css("background-color", "#a9a9a9");
    		$(".text-paragraph1").css("color", "#4f4f4f");
    		$(".text-paragraph2").css("color", "#F2F2F2");
    		$(".text-title").css("color", "#2f80ed");
    	}    	           
    });

    $("#about-div").click(function(){
    	if ($("#about-paragraph").is(":hidden")) {
            $("#about-div").removeClass();
            $("#about-title").removeClass();
            $("#about-div").addClass("row");
            $("#about-div").addClass("rectangle");
            $("#about-div").addClass("small-rectangle")
            $("#about-title").addClass("col-md-4");
            $("#about-title").addClass("text-center");
    		$("#about-paragraph").show();
    	} else {
            $("#about-div").removeClass("row");
            $("#about-title").removeClass("col-md-4");
    		$("#about-paragraph").hide();
    	}
    });

    $("#experience-div").click(function(){
    	if ($("#experience-paragraph").is(":hidden")) {
    		$("#experience-paragraph").show();
    	} else {
    		$("#experience-paragraph").hide();
    	}    	
    });

    $("#skill-div").click(function(){
        if ($("#skill-paragraph").is(":hidden")) {
            $("#skill-div").removeClass();
            $("#skill-title").removeClass();
            $("#skill-div").addClass("row");
            $("#skill-div").addClass("rectangle");
            $("#skill-div").addClass("small-rectangle")
            $("#skill-title").addClass("col-md-4");
            $("#skill-title").addClass("text-center");
            $("#skill-paragraph").show();
        } else {
            $("#skill-div").removeClass("row");
            $("#skill-title").removeClass("col-md-4");
            $("#skill-paragraph").hide();
        }
    });

    $("#education-div").click(function(){
    	if ($("#education-paragraph").is(":hidden")) {
    		$("#education-paragraph").show();
    	} else {
    		$("#education-paragraph").hide();
    	}    	
    });

    $("#backtotop").click(function() {
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;
    });

});