from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
	path('logout', logout_view, name='logout'),
	path('', view_profilepage, name='profilepage'),
]