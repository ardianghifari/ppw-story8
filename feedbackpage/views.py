from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from .forms import Feedback_Form
from .models import Guest

# Create your views here.

def view_feedbackpage(request):
	form = Feedback_Form()
	if request.method == 'POST':
		form = Feedback_Form(request.POST)
		if form.is_valid():
			new_feedback = form.save()
			return HttpResponseRedirect(reverse('profilepage'))
		else:
			form = Feedback_Form()
	return render(request, 'Feedbackpage.html', {'form':form})