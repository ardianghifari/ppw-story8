from django import forms
from .models import Guest

class Feedback_Form(forms.ModelForm):
	name = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
	email = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
	phone_number = forms.CharField(max_length=100, widget=forms.TextInput(attrs={'class':'form-control'}))
	feedback = forms.CharField(required=True, widget=forms.Textarea(attrs={'class':'form-control'}))

	class Meta:
		model = Guest
		fields = ['name', 'email', 'phone_number', 'feedback']