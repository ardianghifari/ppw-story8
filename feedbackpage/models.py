from django.db import models

# Create your models here.

class Guest(models.Model):
	name = models.CharField(max_length=100, blank=True)
	email = models.CharField(max_length=100, blank=True)
	phone_number = models.CharField(max_length=100, blank=True)
	feedback = models.TextField()